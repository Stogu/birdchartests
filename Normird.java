package com.mindklik.characters;

import com.mindklik.helpers.AssetsLoader;

/**
 * Created by MD on 2017-06-30.
 */

public class Normird extends CharacterTemplate {


    @Override
    public void setStats() {

        rotationDeg = 0;
        width = AssetsLoader.NORMID_WIDTH;
        height = AssetsLoader.NORMID_HEIGHT;
        boundsWidth = AssetsLoader.NORMID_BOUNDS_WIDTH;
        boundsHeight = AssetsLoader.NORMID_BOUNDS_HEIGHT;
        rotationSpeed = 130;
        jumpPower = AssetsLoader.NORMID_JUMP_POWER;
        velocityYLimit = 400;
        position.x = (AssetsLoader.CAMERA_WIDTH / 2) - (width / 2);
        position.y = random.nextInt(500) + 250;
        velocity.x = AssetsLoader.NORMID_SPEED;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = -750;


    }

}
