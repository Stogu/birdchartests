package com.mindklik.characters;

import com.badlogic.gdx.Gdx;
import com.mindklik.helpers.AssetsLoader;

/**
 * Created by MD on 2017-07-03.
 */

public class Speerd extends CharacterTemplate {


    @Override
    public void setStats() {

        rotationDeg = 0;
        width = AssetsLoader.SPEERD_WIDTH;
        height = AssetsLoader.SPEERD_HEIGHT;
        boundsWidth = AssetsLoader.SPEERD_BOUNDS_WIDTH;
        boundsHeight = AssetsLoader.SPEERD_BOUNDS_HEIGHT;
        rotationSpeed = 150;
        jumpPower = AssetsLoader.SPEERD_JUMP_POWER;
        velocityYLimit = 550;
        position.x = (AssetsLoader.CAMERA_WIDTH / 2) - (width / 2);
        position.y = random.nextInt(500) + 250;
        velocity.x = AssetsLoader.SPEERD_SPEED;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = -850;
    }
}
