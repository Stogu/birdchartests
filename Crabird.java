package com.mindklik.characters;

import com.badlogic.gdx.Gdx;
import com.mindklik.helpers.AssetsLoader;

/**
 * Created by MD on 2017-07-05.
 */

public class Crabird extends CharacterTemplate {

    @Override
    public void setStats() {

        rotationDeg = 0;
        width = AssetsLoader.CRABIRD_WIDTH;
        height = AssetsLoader.CRABIRD_HEIGHT;
        boundsWidth = AssetsLoader.CRABIRD_BOUNDS_WIDTH;
        boundsHeight = AssetsLoader.CRABIRD_BOUNDS_HEIGHT;
        rotationSpeed = -140;
        jumpPower = AssetsLoader.CRABIRD_JUMP_POWER;
        velocityYLimit = -490;
        position.x = (AssetsLoader.CAMERA_WIDTH / 2) - (width / 2);
        position.y = random.nextInt(500) + 250;
        velocity.x = -AssetsLoader.CRABIRD_SPEED;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = 850;
    }

    @Override
    public void jump() {

        if (Gdx.input.justTouched()) {

            canAddEgg = true;
            position.y -= jumpPower;
            velocity.y = -jumpPower;
            if (width > 0) {
                rotationDeg = -30;
            } else if (width <= 0) {
                rotationDeg = 30;
            }
        }
    }

    @Override
    public void move(float delta) {

        velocity.sub(acceleration.cpy().scl(delta));
        position.sub(velocity.cpy().scl(delta));

        if (velocity.y <= velocityYLimit) {
            velocity.y = velocityYLimit;
        }
    }

    @Override
    public void flip() {

        if (position.x <= AssetsLoader.POLE_WIDTH + boundsWidth / 2 && width < 0) {
            velocity.x = -velocity.x;
            width = -width;
            isForCatFlip = true;
            rotationDeg = -rotationDeg;
            canAddPoint = true;

        } else if (position.x >= AssetsLoader.CAMERA_WIDTH - AssetsLoader.POLE_WIDTH - boundsWidth / 2 && width >= 0) {
            velocity.x = -velocity.x;
            width = -width;
            isForCatFlip = true;
            rotationDeg = -rotationDeg;
            canAddPoint = true;
        }
    }
}

