package com.mindklik.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mindklik.helpers.AssetsLoader;

import java.util.Random;

/**
 * Created by MD on 2017-06-30.
 */

public abstract class CharacterTemplate {


    protected Vector2 position, velocity, acceleration;
    protected int width, height, boundsWidth, boundsHeight;
    protected float rotationDeg, velocityYLimit, rotationSpeed, jumpPower;
    protected boolean isAlive, isForCatFlip, canAddPoint, canAddEgg;
    protected Random random;
    protected Rectangle boundingBox;


    //    declaring those instances cuz we can change dynamically their values in spec. classes ---- becaouse Jsons stats save whole object and when we changes parameter, object has old 'instances'
    public CharacterTemplate() {

        position = new Vector2();
        velocity = new Vector2();
        acceleration = new Vector2();

        random = new Random();

        isAlive = true;
        canAddPoint = false;
        canAddEgg = false;

        boundingBox = new Rectangle();

        setStats();

    }


    public void setStats() {

    }


    //    delta float for scaling some of the values !!!!!
    public void update(float delta) {

        move(delta);
        jump();
        rotate(delta);
        flip();
        doSthElse();
        updateBoundingBox();
    }

    public void move(float delta) {

        velocity.add(acceleration.cpy().scl(delta));
        position.add(velocity.cpy().scl(delta));

        if (velocity.y <= -velocityYLimit) {
            velocity.y = -velocityYLimit;
        }
    }

    private void updateBoundingBox() {

        boundingBox.set(position.x - boundsWidth / 2, position.y + height / 2 - boundsHeight / 2, boundsWidth, boundsHeight);
    }

    public void flip() {

        if (position.x <= AssetsLoader.POLE_WIDTH + boundsWidth / 2 && width <= 0) {
            velocity.x = -velocity.x;
            width = -width;
            isForCatFlip = true;
            rotationDeg = -rotationDeg;
            canAddPoint = true;

        } else if (position.x >= AssetsLoader.CAMERA_WIDTH - AssetsLoader.POLE_WIDTH - boundsWidth / 2 && width > 0) {
            velocity.x = -velocity.x;
            width = -width;
            isForCatFlip = true;
            rotationDeg = -rotationDeg;
            canAddPoint = true;
        }
    }

    public void jump() {

        if (Gdx.input.justTouched()) {

            canAddEgg = true;
            position.y += jumpPower;
            velocity.y = jumpPower;
            if (width > 0) {
                rotationDeg = 30;
            } else if (width <= 0) {
                rotationDeg = -30;
            }
        }

    }

    private void rotate(float delta) {

        if (width > 0) {
            if (rotationDeg > -45) {
                rotationDeg -= rotationSpeed * delta;
            } else {
                rotationDeg = -45;
            }
        } else if (width <= 0) {
            if (rotationDeg < 45) {
                rotationDeg += rotationSpeed * delta;
            } else {
                rotationDeg = 45;
            }
        }

    }

    public void die() {

        isAlive = false;
    }


    public void doSthElse() {
//        System.out.println("super");


    }


    public Vector2 getPosition() {
        return position;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float getRotationDeg() {
        return rotationDeg;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isCanAddPoint() {
        return canAddPoint;
    }

    public void setCanAddPoint(boolean canAddPoint) {
        this.canAddPoint = canAddPoint;
    }

    public boolean isCanAddEgg() {
        return canAddEgg;
    }

    public void setCanAddEgg(boolean canAddEgg) {
        this.canAddEgg = canAddEgg;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public Rectangle getBoundingBox() {
        return boundingBox;
    }
}
