package com.mindklik.characters;

import com.badlogic.gdx.Gdx;
import com.mindklik.helpers.AssetsLoader;

/**
 * Created by MD on 2017-07-01.
 */

public class FaceBird extends CharacterTemplate {


    @Override
    public void setStats() {

        rotationDeg = 0;
        width = AssetsLoader.FACEBIRD_WIDTH;
        height = AssetsLoader.FACEBIRD_HEIGHT;
        boundsWidth = AssetsLoader.FACEBIRD_BOUNDS_WIDTH;
        boundsHeight = AssetsLoader.FACEBIRD_BOUNDS_HEIGHT;
        rotationSpeed = 140;
        jumpPower = AssetsLoader.FACEBIRD_JUMP_POWER;
        velocityYLimit = 450;
        position.x = (AssetsLoader.CAMERA_WIDTH / 2) - (width / 2);
        position.y = random.nextInt(500) + 250;
        velocity.x = AssetsLoader.FACEBIRD_SPEED;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = -850;
    }
}
